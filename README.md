# README

# Steps
1. `docker build -t codegiant:v1 .`
2. `docker compose up -d`
3. `docker compose exec codegiant bash`
4. `rails db:seed`
5. `rails s -b 0.0.0.0`
