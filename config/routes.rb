Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "home#index"

  resources :home do 
    collection do
      get "submit"
      get "successful"
      get "performance"
    end
  end
end
