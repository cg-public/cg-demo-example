Sentry.init do |config|
  config.dsn = "https://ygDfSNM7GJ9EBkysyXL@error.codegiant.io/64"
  config.breadcrumbs_logger = %i[active_support_logger sentry_logger http_logger]

  # To activate performance monitoring, set one of these options.
  # We recommend adjusting the value in production:
  config.traces_sample_rate = 1
  config.enabled_environments = %w[development]
end
