class Quote
  include Mongoid::Document
	include Mongoid::Timestamps

  field :text
  field :author
end