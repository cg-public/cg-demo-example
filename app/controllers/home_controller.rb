require 'uri'
require 'net/http'
class HomeController < ApplicationController
  def index
    @quote = Quote.all.sample
  end

  def submit
    @quote = Quote.new
  end

  def create
    @quote = Quote.create(params.require(:quote).permit(:author, :text))
    redirect_to :successful_home_index
  end

  def performance

    uri = URI.parse("https://api.rapidmock.com/mocks/89mEw")
    requestLength = 2000  # You should replace this with the actual value

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    request = Net::HTTP::Get.new(uri.path)
    request['Cache-Control'] = 'no-cache'
    request['x-rapidmock-delay'] = requestLength

    response = http.request(request)
  end

  def successful
  end

  def testapi
    render :json => {
      "name" => "Harry Bomrah",
      "age" => 30
  }.to_json
  end
end
