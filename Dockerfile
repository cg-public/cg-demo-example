FROM ruby:3.1-bullseye

RUN apt update -y && apt install build-essential git nodejs tzdata -y && \ 
    curl --proto '=https' --tlsv1.2 -sSf https://sh.vector.dev | bash -s -- -y --prefix /usr/local

RUN gem install rails

COPY Gemfile Gemfile.lock ./

RUN bundle install

WORKDIR /codegiant


# RUN bundle

# CMD [ "rails", "s", "-b", "0.0.0.0" ]

