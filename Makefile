docker:
	docker compose up -d

up: docker ps

down:
	docker compose down

ps:
	docker compose ps

logs:
	docker compose logs -f codegiant

enter:
	docker compose exec codegiant bash

restart:
	docker compose restart codegiant

mongo-enter:
	docker compose exec mongo bash

mongo-logs:
	docker compose logs -f mongo

mongo-volume-remove:
	docker volume rm codegiant_mongo_data

build:
	docker build -t codegiant:v1 .

reset: down mongo-volume-remove